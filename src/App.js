
import React, { Component } from "react";
import socketIOClient from "socket.io-client";
import Clock from 'react-clock';
import moment from 'moment-timezone';

class App extends Component {
  constructor() {
    super();
    this.state = {
      response: false,
      endpoint: "ws://ripley-test-backend.herokuapp.com/"
    };
  }

  componentDidMount() {
    const { endpoint } = this.state;
    const socket = socketIOClient(endpoint, { 'forceNew': true });
    
    //socket.on("cities",()=>{console.log("holi")})
    socket.on("cities", data => {this.setState({response: data})});
  }
  
  render() {
    const { response } = this.state;
    console.log(response)
    return (
        <div className="container" style={{ float: "left" }}>
          {response?
            response.map((elem)=>{
              return (
                <div className="city_container">
                  <h3>{elem.city}/{elem.country}</h3>
                  
                  <Clock renderSecondHand={false} value={
                    new Date(new Date().toLocaleString("en-US", {timeZone: elem.darksky_data.time}))
                  }/>

                  <p>{moment().tz(elem.darksky_data.time).format('HH:mm')}</p>
                  <p>{elem.darksky_data.temperatureF}°F/{elem.darksky_data.temperatureC}°C</p>
                </div>
              )
            })
            :
            <p>Loading...</p>}
        </div>
    );
  }
}
export default App;

/**
 * 
 */